# Todo

  - [ ] User accounts and PAM, currently everything is root
  - [ ] Investigating quality of open-source Mesa VC4 performance.
  - [ ] Setting up WPE ecosystem, based on Webkit 2.24.
  - [x] Base system setup, serial consoles, virtual consoles, SSH, etc.

# Getting Started

1a) Get a copy of the buildroot git repository, using the
latest-and-greatest is often the path of least resistance in my
experience.

1b) Get a copy of the WPE Thunder git repo.

2) From inside the buildroot git repo, run the following,
```
make BR2_EXTERNAL=../buildroot-thunder/ list-defconfigs
```
This will show the available configurations, you should see the WPE
thunder configuration listed under the "External configs" section.

3) Select the appropriate thunder configuration, and initialise the
build output directory,
```
make BR2_EXTERNAL=../buildroot-thunder/ O=../build-thunder raspberrypi3-thunder_defconfig
```
`../build-thunder` now contains a `.br-external.mk`, which contains
important path information for using the external root. When
referencing this output dir with future `make` invocations, you no
longer need to pass along `BR2_EXTERNAL`, it is found from the
`.br-external.mk` file.

To configure the created defconfig,

```
make O=../build-thunder menuconfig
```

# Configuration

There is now a baseline configuration. You can type `make
O=../build-thunder` at this point and an image will eventually be
created. However, there are many things you can customise too,

**Compiler caching**. The default location of the compiler cache is by
 default
 `$(BR2_EXTERNAL_WPE_THUNDER_PATH)/../.wpe-thunder-buildroot-ccache`. In
 other words, in a directory above the path to the external WPE
 thunder checkout from step 1b. It works for me, but move it where ever
 you think is best.

**Minimum supported compiler**. I run with GCC 8.3.0. WebKit pushes
  hard to use the latest features of C++, so you must be aware that
  your system should compile with a recent compiler. Relying on older
  compilers will cause you a lot of trouble, please do try to upgrade
  in good time. It's good for you!

**C library**. I default to `glibc`. WebKit does have some
  requirements on the type of C library you pick. `uClibc-ng` does not
  work because of missing features that I don't fully
  understand. `musl` has been reportedly working, but I have not tried
  it.

**Linker**. I enable the `gold` linker in the
  `BR2_BINUTILS_EXTRA_CONFIG_OPTIONS` section by default. This
  improves build times on WebKit empirically. I do not fully
  understand the details, but it Works Better For Me.

**Graphics**. Currently, I am experimenting with the open-source GL
  driver VC4. I use the `mesa3d` package to provide OpenGL API. OpenGL
  ES API is also provided by default using mesa3d. The Raspberry Pi
  foundation has a `rpi-userland` that exposes these APIs in probably
  a "more supported" way, but it's not open-source friendly, it uses
  firmware to control the graphics hardware. It might be the only
  option you have depending on your use case however.

**Init system**. I use `systemd` by default. You can select others if
  you don't like this, but the packages in this external tree depend
  on the `systemd` infrastructure, so you'll create yourself extra
  work.

**Login security**. The default is username root and the password is,
predictably, `root`. **THIS MUST BE CHANGED IN PRODUCTION**.

**Localisation**. I keep the `en_GB` and `C` locales by default
  (`BR2_ENABLE_LOCALE_WHITELIST`). You may want to change that. I
  default install NTP, having the wrong time can give you some
  *really* weird issues.

**Hardware support**. I increased the split between GPU and CPU memory
  to the maximums in favour of the GPU on the 1GB Pi3B+ model.

# Graphics (MESA VC4)
Using the open-source VC4 driver means that graphics options.

The GPU inside the RPI3B+ is COMPLICATED, it runs a custom instruction
set with its own compiler toolchain, has over 100k registers, millions
of lines of code and fairly esteric assembler. The GPU iteself has a
number of internal processors each with different instruction
sets). It's definitely not supposed to be poked at any level lower
than a GLES API for example. It was designed by a company called
Alphamosaic, based in Cambridge, UK, and later bought by Broadcom.


This isn't very friendly to open-source, but the Foundation got a
great deal on the IP, to pass on an affordable SoC that could do
real-time 3D graphics, encode/decode 1080P video and have a camera
ISP.


The rpi-userland GPU driver basically translate down into VCHIQ calls
to the GPU, the open-source userland driver does all this on the ARM
by directly poking registers (which is done by the GPU normally).

VCHIQ and JPEG/H264/MJPEG VC4 interface firmware is in the start.elf file?

Uploading large amounts of data is expensive. Ideally use VBOs or
vertex shaders with small numbers of attributes changing each frame.

By default we triple buffer. An environment variable
V3D_DOUBLE_BUFFER=1 will switch to double buffering.

Texture size max is 2Kx2K.

# Relocatable SDK

If you would like to compile one-off programs, or software that you do
not want to integrate into the Buildroot packages for whatever reason,
you can export the Buildroot toolchain to cross compile and
copy-to-target in whatever manner you see fit.

First create an SDK,

```
make O=../build-thunder sdk
```

And then copy the tarball somewhere convenient,

```
tar cf ~/buildroot-toolchain.tar -C ../build-thunder/host .
```

Your users can now use these toolchain like they would any other. Send
them the tar file, ask them the extract it somewhere, and then inside
the extraced directory run,

```
./relocate-sdk.sh
```

Now it can be used,

```
export PATH=$PATH:/path/to/buildroot-toolchain/bin
arm-linux-c++ arm-hello-world.cpp -o arm-hello-world
scp arm-hello-world target
target$ ./arm-hello-world
Hello, ARM!
```

# Serial Console

You will need an appropriate USB to TTL serial cable. I use [this
one](https://www.adafruit.com/product/954). Check out [this
tutorial](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-5-using-a-console-cable/overview)
to learn how to use it.

Then, see the serial output, connect with screen,

```
sudo screen -L /dev/ttyUSBx
```

Consult the output of `dmesg` to learn what ttyUSB device has been
autoplugged. The `-L` option places a log file of all output in the
directory you launched screen from.

Sometimes I have noticed I can't login to the serial prompt for
unknown reasons, I get garbage like this,

```
buildroot login: root
rPassword: ro
Login incorrect
```

That can happen on the VT over HDMI as well. It looks like a
baud-mismatch, but I haven't debugged it. Things *seem* OK for now.

# Virtual Console
VTs are started on `tty0` and `tty1`. `tty1` should show up on HDMI
output automatically.

# SSH
Dropbear is running automatically.

# Handy commands

This will save changes you made to your output directories .conf file and save it in the thunder repository,
```
make BR2_EXTERNAL=../buildroot-thunder/ O=../build-thunder savedefconfig BR2_DEFCONFIG=../buildroot-thunder/configs/raspberrypi3-thunder_defconfig 
```

# Kernel Configuration

NFS root example
```
console=ttyAMA0,115200 console=tty1 root=/dev/nfs rootfstype=nfs nfsroot=192.168.1.115:/home/<xxx>/nfs,nolock nfsrootdebug smsc95xx.turbo_mode=N ip=dhcp elevator=deadline rootwait init=/sbin/init
```

# Developing with Buildroot

Place a file named `local.mk` in your `$(CONFIG_DIR)`. You can then
override the src locations of packages you are developing,

For example, in my out-of-tree build setup
```
$ cat ../build-thunder/local.mk
COG_OVERRIDE_SRCDIR = /home/cturner/git/cog
WPEBACKEND_FDO_OVERRIDE_SRCDIR = /home/cturner/git/WPEBackend-fdo
#WPEWEBKIT_OVERRIDE_SRCDIR_RSYNC_EXCLUSIONS = \
#        --exclude JSTests --exclude ManualTests --exclude PerformanceTests \
#        --exclude WebDriverTests --exclude WebKitBuild --exclude WebKitLibraries \
#        --exclude WebKit.xcworkspace --exclude Websites --exclude Examples
```

# Frequently encountered issues

**Keyboard not working**. I bought an iPazzPort wireless combo
  keyboard/trackpad from Amazon. This is a highly flaky device, and
  despite being recognised by the USB stack in Linux, it doesn't
  "work". The fix for me was to switch to a Logitec wired keyboard.
